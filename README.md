# MoodLander

## Zadání

Hra MoonLander je simulátor přistání na Měsíci. Cílem hry je přistát na povrchu Měsíce, vyhnout se meteoritům, nesjet z trasy a nespotřebovat všechno palivo.

## Kompilace programu

Hra by měla být ihned připravena ke spuštění. Chcete-li hru spustit, musíte přejít do souboru main.cpp a za metodou main stisknout tlačítko run.

## Spuštění a přepínače
```c
___________
|---------|
|         |
|         |
|         |
|    8    |
|         |
|         |
|     6   |
| 6   6 6 |
___________
Height: 43
Fuel: 20
```


Toto je příklad spuštěné hry, kde můžete vidět Rocket [8], Meteors [6], Borders [|],[-],[_] a rozhraní Fuel a Height.

## Ovládání programu

- W nebo u: Pohyb nahoru o 1 jednotku za 1 palivo.
- A nebo l: Pohyb o 1 jednotku doleva a 1 jednotku dolů za 1 palivo.
- S nebo d: Pohyb o 2 jednotky dolů bez spotřeby paliva.
- D nebo r: Pohyb o 1 jednotku doprava a 1 jednotku dolů za 1 palivo.
- F nebo f: Přidání 5 paliva.
- H nebo h: Zobrazení nápovědy.
- Q: Ukončení hry.

## Testování programu

1. `A->A->A->A->A`
```c
___________
|         |
|         |
|     6   |
| 6   6 6 |
|   66    |
|6  6    6|
|       6 |
|  6  6   |
|       66|
___________
Height: 38
Fuel: 15
The rocket went off course and crashed.
You lost.
```

2. `S->S->D->S->D->S->S->A->A->D->S->D->S->A->A->S->W->S->D->S->W->S->D->D->S->D->S->A->S->S->A->S`
```c
___________
|  6 6 6  |
| 6   6  6|
|      6  |
| 6       |
|  6   6  |
|6      66|
|      8  |
|000000000|
|000000000|
___________
Height: 0
Fuel: 4
We have successfully landed on the moon. And it turned out to be made of cheese.
You won and had a great fondue.
```
3. `S->D`
```c
___________
|         |
|         |
|         |
|         |
|     6   |
| 6   6 6 |
|   66    |
|6  6    6|
|       6 |
___________
Height: 40
Fuel: 19
A rocket collided with a meteor and crashed.
You lost.
```
